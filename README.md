## very small caddy-proxy container but still have a working s6-overlay process and socklog-overlay


[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/caddy-gen.svg)](https://hub.docker.com/r/cogentwebs/caddy-gen/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/caddy-gen.svg)](https://hub.docker.com/r/cogentwebs/caddy-gen/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/caddy-gen.svg)](https://hub.docker.com/r/cogentwebs/caddy-gen/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/caddy-gen.svg)](https://hub.docker.com/r/cogentwebs/caddy-gen/)

This is a very small caddy container but still have a working s6-overlay process and socklog-overlay .